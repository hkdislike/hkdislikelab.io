# @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
# @source https://gitlab.com/hkdislike/hkdislike.gitlab.io/-/blob/master/Makefile
# @source https://gitlab.com/hkdislike/dev/-/blob/dev/Makefile


.PHONY: all clean preview

all: index.html uglified.js

index.html: home-page
	guile home-page > index.html

uglified.js: source.js
	perl -0777 -ne 'print m!(^(?:// @[^\n]+\n)+)!' source.js > uglified.js
	python3 -m jsmin source.js >> uglified.js
	perl -0777 -ne 'print m!((?:// @[^\n]+\n)+$$)!' source.js >> uglified.js

clean:
	rm -f index.html uglified.js

preview: all
	firefox --private-window index.html


# @license-end
